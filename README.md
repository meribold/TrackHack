TrackHack
=========

Simple and fast semi-automatic single particle tracking software.  Designed to track cells
in sequences of phase shift images obtained using digital holographic microscopy.  Only
grayscale bitmaps are supported as input.

Written in C++ using some Boost libraries (Filesystem, Regex, Thread) and wxWidgets.
Tested on GNU/Linux and Windows (compiled using MSYS2 and MinGW-w64).

Developed at the University of Münster.

<!---
For Windows, you can download the most recent executable along with some required
libraries
[here](https://www.dropbox.com/s/xe9da1712u1ntws/track_hack_2015-04-01.zip?dl=1).  It
should look something like this:
-->

#### GNU/Linux Screenshot
![Screenshot](https://www.dropbox.com/s/034xgpzxozqk8kz/track-hack-screenshot-arch.png?dl=1)

#### Windows Screenshot
![Screenshot](https://www.dropbox.com/s/6cf76dneg1dgt5n/track-hack-screenshot-windows-7.png?dl=1)

<!--- vim: set tw=90 sts=4 sw=4 et spell: -->
